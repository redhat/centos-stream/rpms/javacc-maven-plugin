# javacc-maven-plugin

A [maven](https://maven.apache.org/) plugin for processing
[JavaCC](https://javacc.github.io/javacc/) grammar files.
